<?php
namespace Rentateam\Queue;

use Rentateam\Queue\Adapter\AdapterInterface;
use Rentateam\Queue\Exception\QueueException;
use Rentateam\Queue\Packer\PackerInterface;

abstract class AbstractQueue
{
    /** @var AdapterInterface */
    protected $adapter;

    /** @var PackerInterface */
    protected $packer;

    /**
     * @param AdapterInterface $adapter
     * @param PackerInterface $packer
     */
    public function __construct(AdapterInterface $adapter, PackerInterface $packer)
    {
        $this->adapter = $adapter;
        $this->packer = $packer;
    }

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @param $message
     * @return boolean
     */
    abstract public function isValid($message);

    /**
     * @param $message
     * @return bool
     * @throws QueueException
     */
    public function push($message)
    {
        if (false === $this->isValid($message)) {
            throw new QueueException('Некорректный тип');
        }

        return $this->adapter->push($this->getName(), $this->packer->pack($message));
    }

    /**
     * @return null
     */
    public function pop()
    {
        $string = $this->adapter->pop($this->getName());
        return $string ? $this->packer->unpack($string) : null;
    }
}