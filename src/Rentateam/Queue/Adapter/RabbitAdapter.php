<?php

namespace Rentateam\Queue\Adapter;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitAdapter implements AdapterInterface
{
    /** @var \PhpAmqpLib\Channel\AMQPChannel */
    protected $channel;

    /**
     * RabbitAdapter constructor.
     * @param AMQPStreamConnection $connection
     */
    public function __construct(AMQPStreamConnection $connection)
    {
        $this->channel = $connection->channel();

    }

    /**
     * @param string $name Name of queue
     * @param string $string Message after packer
     * @return bool
     */
    public function push($name, $string)
    {
        try {
            $this->channel->queue_declare($name, false, true, false, false);
            $this->channel->basic_publish(new AMQPMessage($string), '', $name);
        } catch (\Throwable $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $name Name of queue
     * @return mixed
     */
    public function pop($name)
    {
        $message = $this->channel->basic_get($name, true);
        return $message ? $message->getBody() : null;
    }

    public function __destruct()
    {
        $this->channel->close();
    }
}