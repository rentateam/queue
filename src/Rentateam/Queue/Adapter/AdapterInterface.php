<?php
namespace Rentateam\Queue\Adapter;

interface AdapterInterface
{
    /**
     * @param string $name
     * @param string $string
     * @return bool
     */
    public function push($name, $string);

    /**
     * @param string $name
     * @return mixed
     */
    public function pop($name);
}