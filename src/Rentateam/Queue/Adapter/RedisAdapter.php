<?php
namespace Rentateam\Queue\Adapter;

class RedisAdapter implements AdapterInterface
{
    protected $rediska;
    protected $list;

    /**
     * @param \Rediska $rediska
     */
    public function __construct(\Rediska $rediska)
    {
        $this->rediska = $rediska;
    }

    /**
     * @param string $name
     * @return \Rediska_Key_List
     */
    protected function getList($name)
    {
        if (null === $this->list) {
            $this->list = new \Rediska_Key_List($name);
        }

        return $this->list;
    }

    /**
     * @param string $name
     * @param string $string
     * @return bool
     */
    public function push($name, $string)
    {
        return $this->getList($name)->prepend($string);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function pop($name)
    {
        return $this->getList($name, $name)->pop();
    }
}
