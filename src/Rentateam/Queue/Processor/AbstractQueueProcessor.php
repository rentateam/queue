<?php
namespace Rentateam\Queue\Processor;

use Rentateam\Queue\AbstractQueue;

abstract class AbstractQueueProcessor
{
    protected $queue;

    /**
     * @param AbstractQueue $queue
     */
    public function __construct(AbstractQueue $queue)
    {
        $this->queue = $queue;
    }

    public function process()
    {
        while ($message = $this->queue->pop()) {
            $this->work($message);
        }
    }

    abstract public function work($message);
}