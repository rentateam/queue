<?php
namespace Rentateam\Queue\Packer;

class SimplePacker implements PackerInterface
{
    /**
     * @param $object
     * @return string
     */
    public function pack($object)
    {
        return base64_encode(serialize($object));
    }

    /**
     * @param string $string
     * @return mixed
     */
    public function unpack($string)
    {
        return unserialize(base64_decode($string));
    }
}