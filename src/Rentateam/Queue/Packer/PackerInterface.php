<?php
namespace Rentateam\Queue\Packer;

interface PackerInterface
{
    /**
     * @param $object
     * @return string
     */
    public function pack($object);

    /**
     * @param string $string
     * @return mixed
     */
    public function unpack($string);
}